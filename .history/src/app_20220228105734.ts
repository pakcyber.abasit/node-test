
const createError = require('http-errors');
import express = require('express') ;
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const indexRouter = require('./routes/router.js');
const loginRouter = require('./routes/login.js');
 
const app = express();
 
app.use(express.json());
 
app.use(bodyParser.json());
 
app.use(bodyParser.urlencoded({
    extended: true
}));


app.use(function (req, res, next) {
    res.contentType('application/json');
    next();
});

 
app.use(cors());
 
app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/registration', indexRouter);
 
app.listen(3000,() => console.log('Server is running on port 3000'));