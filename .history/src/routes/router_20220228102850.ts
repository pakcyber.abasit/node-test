import express = require('express');
const router = express.Router();
// const db  = require('./dbConnection');
// const { signupValidation } = require('./validation');
const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
 
router.post('/registration', (req, res, next) => {


  let name = req.body.name;
  let password = req.body.password;
  let email = req.body.email;
  
  if(!name){
    res.send({ message: "Name is required ..." });
  }else {
      for( let i = 0; i < name.length; i++){
        if(!isNaN(name.charAt(i)) && !(name.charAt(i) === " ") ){
          res.send({ message: "Name Must be Sting..." });
        }
      }
  }
  if (!password){
    res.send({ message: "Passwaord is required ..." });
  }else{
    var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
    var minNumberofChars = 6;
    var maxNumberofChars = 16;
    if(password.length < minNumberofChars){
      res.send({ message: "Passwaord should contain six character" });
    }
    if(!regularExpression.test(password)) {
        res.send({ message: "password should contain atleast one number and one special character"});
    }
  } 
  if(!email){
    res.send({ message: "Email is required ..." });
  }else{
    var emai_regex = /\S+@\S+\.\S+/;
    if(!emai_regex.test(email)){
      res.send({ message: "Invalid Email" });
    }
  }


  let data = {
    name : name,
    password : password,
    email :email
  }
  res.send({ message: data});






});


router.get('/',(req,res)=>{
  res.send('Hello Worlds!');
})
 
 
module.exports = router;