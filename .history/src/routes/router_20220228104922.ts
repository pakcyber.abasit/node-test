import express = require('express');
const router = express.Router();
// const db  = require('./dbConnection');
// const { signupValidation } = require('./validation');
const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
 
router.post('/registration', (req, res, next) => {

  let name = req.body.name;
  let password = req.body.password;
  let email = req.body.email;
  
  if(!name){
    res.status(400);
    res.send({ message: "Name is required ..." });
  }else {
    for( let i = 0; i < name.length; i++){
      if(!isNaN(name.charAt(i)) && !(name.charAt(i) === " ") ){
          res.status(400);
          res.send({ message: "Name Must be Sting..." });
        }
      }
    }
    if (!password){
    res.status(400);
    res.send({ message: "Passwaord is required ..." });
  }else{
    var regularExpression = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    var minNumberofChars = 6;
    var maxNumberofChars = 16;
    if(password.length < minNumberofChars){
      res.status(400);
      res.send({ message: "Passwaord should contain six character" });
    }
    if(regularExpression.test(password)) {
      res.status(400);
      res.send({ message: "password should contain atleast one number"});
    }                                   
  } 
  if(!email){
    res.status(400);
    res.send({ message: "Email is required ..." });
  }else{
    var emai_regex = /\S+@\S+\.\S+/;
    if(!emai_regex.test(email)){
      res.status(400);
      res.send({ message: "Invalid Email" });
    }
  }
  if(name && password && email){
    res.status(200);
    res.send({ message: "Register Successfully" });
  }else{
    let data = {
      name : name,
      password : password,
      email :email
    }
    res.status(200);
    res.send({ message: data});
  }
});
router.post('/login',(req,res)=>{
  let email = req.body.email;
  let password = req.body.password;
  if(!email && !password){
    res.status(400);
    res.send({ message: "Email and Password is required .. "});
  }
  if(email){
    var emai_regex = /\S+@\S+\.\S+/;
    if(!emai_regex.test(email)){
      res.status(400);
      res.send({ message: "Invalid Email" });
    }
  }else if(password){
    var regularExpression = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    var minNumberofChars = 6;
    var maxNumberofChars = 16;
    if(password.length < minNumberofChars){
      res.status(400);
      res.send({ message: "Passwaord should contain six character" });
    }
    if(regularExpression.test(password)) {
      res.status(400);
      res.send({ message: "password should contain atleast one number"});
    }                                   
  }else{
    let data = {
      Email : email,
      Password : password
    }
    res.status(200);
    res.send({ message: data});
    
  }








})


router.get('/',(req,res)=>{
  res.send('Hello Worlds!');
})
 
 
module.exports = router;