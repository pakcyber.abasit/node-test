import express = require('express');
const router = express.Router();

router.get('/',(req,res)=>{
  res.send({message: "hello"});
})

router.post('/login/user',(req,res,next) => {
  let email = req.body.email;
  let password = req.body.password;
  
  if(!email && !password){
    res.status(400);
    res.send({ message: "Email and Password is required .. "});
  }
  if(email){
    var emai_regex = /\S+@\S+\.\S+/;
    if(!emai_regex.test(email)){
      res.status(400);
      res.send({ message: "Invalid Email" });
    }
  }else if(password){
    var regularExpression = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    var minNumberofChars = 6;
    var maxNumberofChars = 16;
    if(password.length < minNumberofChars){
      res.status(400);
      res.send({ message: "Passwaord should contain six character" });
    }
    if(regularExpression.test(password)) {
      res.status(400);
      res.send({ message: "password should contain atleast one number"});
    }                                   
  }else{
    let data = {
      Email : email,
      Password : password
    }
    res.status(200);
    res.send({ message: data});
    
  }
})

module.exports = router;